﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpoPatterns.Observer
{
    abstract class ASubject
    {
        ArrayList list = new ArrayList();

        public delegate void StatusUpdate(float price);
        public event StatusUpdate OnStatusUpdate = null;

        public void Attach(Shop product)
        {
            list.Add(product);
        }
        public void Detach(Shop product)
        {
            list.Remove(product);
        }

        public void Attach2(Shop product)
        {
            OnStatusUpdate += new StatusUpdate(product.Update);
        }
        public void Detach2(Shop product)
        {
            OnStatusUpdate -= new StatusUpdate(product.Update);
        }

        public void Notify(float price)
        {
            foreach (Shop p in list)
            {
                p.Update(price);
            }

            if (OnStatusUpdate != null)
            {
                OnStatusUpdate(price);
            }
        }
    }
    class DummyProduct : ASubject
    {
        public void ChangePrice(float price)
        {
            Notify(price);
        }
    }
    interface IObserver
    {
        void Update(float price);
    }
    class Shop : IObserver
    {
        //Name product
        string name;
        float price = 0.0f; 

        public Shop(string name)
        {
            this.name = name;
        }
        #region IObserver Members

        public void Update(float price)
        {
            this.price = price;

            Console.WriteLine("Price at {0} is now {1}", name, price);
        }

        #endregion
    }
    class Program
    {
        static void Main(string[] args)
        {
            DummyProduct product = new DummyProduct();

            Shop shop1 = new Shop("Shop 1");
            Shop shop2 = new Shop("Shop 2");
            Shop shop3 = new Shop("Shop 3");
            Shop shop4 = new Shop("Shop 4");

            product.Attach(shop1);
            product.Attach(shop2);
            product.Attach2(shop3);
            product.Attach2(shop4);

            product.ChangePrice(23.0f);

            product.Detach(shop2);
            product.Detach2(shop4);

            product.ChangePrice(26.0f);

            Console.WriteLine("end");
            Console.ReadKey();
        }
    }
}
